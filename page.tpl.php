<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<body>

<div id="header">

<div class="headerleft">
	<div id="logowrapper">
        <?php if ($site_name) { ?><div id="siteName">
            <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a>
          </div><?php } ?>
        <?php if ($site_slogan) { ?><p><?php print $site_slogan ?></p><?php } ?>
      </div>
	</div>

<div class="headerright">
	<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
</div>

</div>
<div id="content">

	<div id="contentleft">			
		<?php print $content ?>
	</div>

	
<!-- begin r_sidebar -->

	<div id="r_sidebar">
			<?php print $right ?>
			<?php print $left ?>
	</div>

<!-- end r_sidebar -->
</div>

<!-- The main column ends  -->

<!-- begin footer -->

<div style="clear:both;"></div>


<div id="footerbg">

	<div id="footer">
		<?php print $footer ?>
	</div>

</div>

<?php print $closure ?>
</body>
</html>